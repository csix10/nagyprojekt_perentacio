

import os
import requests

import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import plotly.graph_objects as go
import seaborn as sns

g_drive_id_immigrants_emigrants_by_destination2 = '1z2FCmHFFaV0bM8BwScWL71ZsZ2OEqdiF'
g_drive_id_population = '15y84g6vsrcnNYq1QMfbFtYiNJGgqLkzp'
g_drive_id_accidents_2017 = '1X9hA7S5Z6A0jbEW3myWP1dCOBQ4R98jf'
g_drive_url = 'https://drive.google.com/uc?export=download&id='

class DataLoader:
  def __init__(self, file_url, file_name):
    # a mappa, ahová letöltünk
    data_folder = "data"
    os.makedirs(data_folder, exist_ok=True)
    # adatfájl teljes elérési útja
    data_file = os.path.join(data_folder, file_name)
    # internetről letöltés
    r = requests.get(file_url, allow_redirects=True)
    open(data_file, "wb").write(r.content)
    # letöltött fájl beolvasása
    # a betöltött adatot a `data` nevű változóban tároljuk
    self.data = pd.read_csv(data_file)

file_url_immigrants_emigrants_by_destination2 = g_drive_url + g_drive_id_immigrants_emigrants_by_destination2
file_url_population = g_drive_url + g_drive_id_population
file_url_accidents_2017 = g_drive_url + g_drive_id_accidents_2017

iebd2 = DataLoader(file_url=file_url_immigrants_emigrants_by_destination2, 
                   file_name="immigrants_emigrants_by_destination2.csv")
popul = DataLoader(file_url=file_url_population, 
                   file_name="population.csv")
accid = DataLoader(file_url=file_url_accidents_2017, 
                   file_name="accidents_2017.csv")

districts=popul.data.groupby('District.Name', as_index=False).count() #Barcelona kerületei

class Calculator:
    def __init__(self, data):
        self.data = data

    def table_filter(self, table_1, column_1, column_2): # egyesít két adat táblát a megadott oszlopok alapján
        return pd.merge(right=table_1, left=self.data, left_on=column_2, right_on=column_1)

    def age_tree(self): # létrehozza a korfa adattáblázatát
        condition_1 = (self.data['Gender'] == 'Female') \
                      & (self.data['Year'] == 2017)
        women = self.data[condition_1].groupby('Age',
                                               as_index=False,
                                               sort=False).sum(numeric_only=True)
        condition_2 = (self.data['Gender'] == 'Male') \
                      & (self.data['Year'] == 2017)
        men = self.data[condition_2].groupby('Age',
                                             as_index=False,
                                             sort=False).sum(numeric_only=True)
        women['Number'] = - women['Number']
        return pd.DataFrame({'Nok': women['Number'][::-1],
                             'Ferfiak': men['Number'][::-1],
                             'Beosztas': women['Age'][::-1]})

    def accident(self): # rendezi a balesetekhez kapcsolódó adatokat
        order = list(range(5, -1, -1))+list(range(23, 5, -1))
        table=self.data.groupby('Hour').count()['Id']
        return  table.iloc[order]

    def migration_sankey(self, table):  # olyan adattáblát hoz létre ahol a földrajzi helyeket számokra cseréli le                    
        source = table.groupby('from', 
                               as_index=False, 
                               sort=False).sum(numeric_only=True)[['from', 'weight']]
        target = table.groupby('to', 
                               as_index=False, 
                               sort=False).sum(numeric_only=True)[['to', 'weight']]
        
        table_index=pd.merge(right=table, 
                             left=source.reset_index(), 
                             left_on='from', 
                             right_on='from')[['index','to','weight_y']]
        table_index.rename(columns={'index': 'from_index'}, 
                           inplace=True)
        
        table_index=pd.merge(right=table_index, 
                             left=target.reset_index(), 
                             left_on='to', 
                             right_on='to')[['index', 'from_index', 'weight_y']]
        table_index.rename(columns={'index': 'to_index'}, 
                           inplace=True)
        
        table_index['to_index']=table_index['to_index']+len(source)
        return table_index

class Analyzer:
    def __init__(self, calc):
        self.calc = calc
    
    def plot_emigration(self, table_1): # létre hozza a kivándorlásokat szemléltető ábrát
        table = self.calc.table_filter(table_1, 
                                       'District.Name', 
                                       'from')[['from', 'to', 'weight']]
        source = table.groupby('from', 
                               as_index=False, 
                               sort=False).sum(numeric_only=True)[['from', 'weight']]
        target = table.groupby('to', 
                               as_index=False, 
                               sort=False).sum(numeric_only=True)[['to', 'weight']]

        labels = list(source['from']) + list(target['to'])

        table_index=self.calc.migration_sankey(table=table)
        
        fig = go.Figure(data=[go.Sankey(
            node=dict(
                pad=15,
                thickness=20,
                line=dict(color="black", width=0.5),
                label=labels,
                color="red"
            ),
            link=dict(
                source=table_index['from_index'],
                target=table_index['to_index'],
                value=table_index['weight_y']
            )
        )])

        fig.update_layout(title_text="Kivándorlások Barcelonából", font_size=10)
        fig.show()
        
    def plot_immigration(self, table_1): # létre hozza a bevándorlásokat szemléltető ábrát
        table = self.calc.table_filter(table_1, 
                                       'District.Name', 
                                       'to')[['from', 'to', 'weight']]
        source = table.groupby('from', 
                               as_index=False, 
                               sort=False).sum(numeric_only=True)[['from', 'weight']]
        target = table.groupby('to', 
                               as_index=False, 
                               sort=False).sum(numeric_only=True)[['to', 'weight']]

        labels = list(source['from']) + list(target['to'])

        table_index=self.calc.migration_sankey(table=table)
        
        fig = go.Figure(data=[go.Sankey(
            node=dict(
                pad=15,
                thickness=20,
                line=dict(color="black", width=0.5),
                label=labels,
                color="red"
            ),
            link=dict(
                source=table_index['from_index'],
                target=table_index['to_index'],
                value=table_index['weight_y']
            )
        )])

        fig.update_layout(title_text="Kivándorlások Barcelonából", font_size=10)
        fig.show()

    def plot_age_tree(self): # létre hozza a korfát szemléltető ábrát
        table = self.calc.age_tree()

        bar_plot = sns.barplot(x='Nok', 
                               y='Beosztas', 
                               data=table, 
                               order=table['Beosztas'], 
                               lw=0, 
                               color="red",
                               label='Nők')
        bar_plot = sns.barplot(x='Ferfiak', 
                               y='Beosztas', 
                               data=table, 
                               order=table['Beosztas'], 
                               lw=0, 
                               color="blue",
                               label='Férfiak')

        bar_plot.set(xlabel="Barcelona népessége", 
                     ylabel="Kor beosztás", 
                     title="Barcelona korfája 2017-ben")

        legend_labels = ["Férfiak", "Nők"]
        legend_colors = ["blue", "red"]
        plt.legend()
        plt.show()

    def plot_accident(self): # létre hozza a bale szemléltető ábrát
        table = self.calc.accident()
        
        plt.figure(figsize=(8, 8))
        ax = plt.subplot(111, polar=True)
        plt.axis('off')

        height = ((table.max() - 1) / table.max()) * table + 1
        width = 2 * np.pi / len(table.index)
        indexes = list(range(1, len(table.index) + 1))
        angles = [element * width for element in indexes]

        start_color = np.array([0, 0, 255])
        end_color = np.array([255, 127, 0])

        colors = np.concatenate((np.round(np.linspace(start_color, end_color, 12)).astype(int), np.round(np.linspace(end_color, start_color, 12)).astype(int)))
        colors = np.concatenate((colors[-6:], colors[:-6]))

        hex_colors = []
        for color in colors:
            hex_colors.append('#{:02x}{:02x}{:02x}'.format(*color))

        bars = ax.bar(
            x=[element * width for element in indexes],
            height=height,
            width=width,
            bottom=1,
            linewidth=1,
            color=hex_colors,
            edgecolor="white"
        )
        
        labelPadding = 50

        for bar, angle, height, i in zip(bars, angles, height, table.index):

            label = str(i) + " óra"
            rotation = np.rad2deg(angle)

            alignment = ""
            if np.pi / 2 <= angle < 3 * np.pi / 2:
                alignment = "right"
                rotation = rotation + 180
            else:
                alignment = "left"

            ax.text(
                x=angle,
                y=1 + bar.get_height() + labelPadding,
                s=label,
                ha=alignment,
                va='center',
                rotation=rotation,
                rotation_mode="anchor")

"""##Első feladat: Barcelonából való kivandorlás és bevándorlás

###Az ábrán a kivondorlók számát látjuk a Barcelona kerületei és úticéljuk szerinti bontásban.
"""

calc_iebd2 = Calculator(data=iebd2.data)
analyzer_iebd2 = Analyzer(calc=calc_iebd2)

analyzer_iebd2.plot_emigration(districts)

"""A kivándorlók nagyából egységesen kerültek elő Barcelona kerületeiből, azonban az ábrán szembetűnő, hogy az uticélok közül kimagaslik Katalonia. Katalonia pedig Spanyolországnak az a területe aminek központjában Barcelona van. Úgy, hogy Barcelonára jellemző a szuburbanizáció ami anyit tesz, hogy a város lakosság jelentős része a város agglomerációjába kiköltözik.

###Az ábrán Barcelonába való bevándorlók számát mutatja, eredeti otthonuk és  Barcelona kerületeinek bontásában.
"""

analyzer_iebd2.plot_immigration(districts)

"""Az ábrán jól látszik, hogy a bevándorlók többsége Kataloniából és külföldről érkeznek és nagyából egyenlő arányban telepednek le Barcelona kerületeiben. Azonban ha összevetjük a Kataloniából való bevándorlókat az óda kivándorlókal akkor azt tapasztaljuk, hogy a kivándorlás nagyobb. Úgy hogy Barcelona néppessége csökken, Barcelona agglomerációjának javára. És észrevehető hogy a külföldi bevándorlók aránya nagyon magas a külföldre kiköltőzökhöz képest.

##Második feladat: Barcelona népessége

###Az ábrán Barcelona korfája látható.
"""

calc_popul = Calculator(data=popul.data)
analyzer_popul = Analyzer(calc=calc_popul)

analyzer_popul.plot_age_tree()

"""Észrevehető a fejlett országokra jellemző urna alak. Így Barcelona népességét vizsgálva előregedés és fogyás állapítható meg. Kevés gyerek születik és magas az idősek aránya. Így az egy munkaképes korúra jutó eltartottak száma magas. Észre vehető hogy idősebb korosztályokban nagyobb a nők aránya mint a férfiaké, ennek biológiai okai vannak.

##Harmadik feladat: Barcelonában történt balesetek

###Az ábra Barcelonában egy év alatt történt összes balesett számát mútatja órákra lebontva.
"""

calc_accid = Calculator(data=accid.data)
analyzer_accid = Analyzer(calc=calc_accid)

analyzer_accid.plot_accident()

"""Legtöbb balesett reggelicsúcs, ebédidőben és esticsúcs alatt történt. Éjszakai balesetek száma jóval kisebb a nappali balesetek számához képest. Ez azzal magyarázható, hogy éjszaka sokkal csekélyebb a forgalom mint nappal. Illetve a legtöbb baleset a napi csúcsforgalmakban történt, mikor legnagyobb a forgalom. Így megállapítható hogy a balesetek száma a forgalom nagyságával egységesen változik nő vagy csökken."""